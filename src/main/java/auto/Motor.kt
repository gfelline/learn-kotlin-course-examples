package auto

class Motor(val ps:Int, val anzZylinder:Int) {

    fun schalteEin() {
        print("""Brum Brum...
            | $anzZylinder Zylinder Motor mit $ps Ps ist gestartet!
        """.trimMargin())

    }


    fun schalteAus() {
        print("""Plump...
            | $anzZylinder Zylinder Motor mit $ps Ps ist gestoppt!
        """.trimMargin())
    }

    fun beschleunige(geschwindigkeit:Int) {
        print("""Hyyyyyuuuee...
            | $anzZylinder Zylinder Motor mit $ps Ps beschleunigt auf $geschwindigkeit km/h Geschwindigkeit!
        """.trimMargin())
    }

}