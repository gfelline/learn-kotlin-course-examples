package auto

class Auto(val marke:String, val motor:Motor) {


    fun fahreGemuetlichZumZiel() {
        println("$marke wird gestartet!!!!")
        motor.schalteEin()

        println("$marke fährt los!!!!")
        motor.beschleunige(60)

        println("$marke wird ausgeschaltet!!!!!!!")
        motor.schalteAus()
    }

    fun fahreSchnellZumZiel() {

    }
}