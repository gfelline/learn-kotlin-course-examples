package vererbung

fun main(args: Array<String>) {

    /*
    val footballer = Footballer("Walter")
    footballer.move(10)
    BusinessMan("Fritz").walk()
    val teacher = MathTeacher("Susanne")
    teacher.move(15)
    Undercover().walk()

    Footballer("Trudi").playFootball()
    BusinessMan("Cosimo").runBusiness()
    MathTeacher("Sabrina").teachMath()
    Undercover().doSecretStuff()
    */

    val personen = arrayListOf(
            Footballer("Walter"),
            MathTeacher("Susanne"),
            Undercover(),
            Footballer("Trudi"),
            BusinessMan("Cosimo"),
            MathTeacher("Sabrina")
    )

    val spielfigurMover = SpielfigurMover()
    spielfigurMover.move(personen, 23)

    val footballer = personen.filter { person ->
        person is Footballer
    }

    spielfigurMover.move(footballer, 33)

    for (fb in footballer) {
        if (fb is Footballer) {
            fb.playFootball()
        }

    }




}

class SpielfigurMover {
    fun move(spielfiguren: List<Person>, newPosition:Int) {

        for (person in spielfiguren) {
            person.move(newPosition)
        }

    }
}

open class Person(val name: String) {

    private var position: Int = 0

    fun move(pos: Int) {
        position += pos
        println("Neue Position des Spielers $name ist neu $position")
    }

    fun talk() {
        println("...talk talk")
    }

    open fun walk() {
        println("...walk walk")
    }
}

class MathTeacher(name: String) : Person(name) {

    fun teachMath() {
        println("...teachMath")
    }

    override fun walk() {
        println("begin walk")
        super.walk()
        println("...walk as teacher")
        println("end walk")
    }
}

class Footballer(name: String) : Person(name) {

    fun playFootball() {
        println("...playFootball")
    }

}

class BusinessMan(name: String) : Person(name) {

    fun runBusiness() {
        println("...runBusiness")
    }
}

//    [ ]       -->   []
class Undercover : Person("Mister X") {

    fun doSecretStuff() {
        println("...doSecretStuff")
    }
}