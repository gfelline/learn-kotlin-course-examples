import java.lang.IllegalStateException

fun askLanguage() : String {
    println("Bitte, geben Sie eine Sprache ein!")
    return readLine() ?: throw IllegalStateException("Error!")
}

fun greet(language:String) : Boolean {


    // Variante mit if
    if (language == "Deutsch") {
        println("Guten Morgen!")
        return true
    } else if (language == "Englisch") {
        println("Good Morning!")
        return true
    } else {
        println("Leider kenne ich die Sprache $language nicht!")
        return false
    }
}

fun main(args: Array<String>) {

    do  {
        var greetingSucess = greet(askLanguage())
    } while (!greetingSucess)


}




