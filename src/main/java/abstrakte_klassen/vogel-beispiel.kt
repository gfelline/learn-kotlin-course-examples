package abstrakte_klassen

abstract class Tier {

    fun atmen() {
        println("ich atme")
    }

}

abstract class Vogel : Tier() {

    protected var markierung:Int? = null

    abstract fun fliegen()

    fun markiere(eineMarkierung: Int) {
        markierung = eineMarkierung
        println("Ou krass, ich wurde gerade mit der Ziffer $eineMarkierung markiert!")
    }

}

class Bingo(val tier:Tier) {
    init {
        tier.atmen()
    }
}

class Ente : Vogel() {

    init {
        markierung = 42
    }

    override fun fliegen() {
        println("ich fliege : quak quak quak")
    }

    fun erlegt() {
        println("Okay, es ist halt so - nun bin ich erlegt....")
    }
}

class Kolibri : Vogel() {

    override fun fliegen() {
        println("ich fliege : rirriririii")
    }
}

class Taube : Vogel() {

    override fun fliegen() {
        println("ich fliege : gur gur gur")
    }
}

class Ornithologe {

    fun markiereVogel(vogel: Vogel, markierung:Int) {
        vogel.markiere(markierung)
    }
}

class Jaeger {

    fun jage(ente:Ente) {
        ente.erlegt()
    }
}


fun main(args: Array<String>) {

    val voegel = arrayListOf(Ente(),Taube(),Ente(),Taube(), Kolibri())

    val max = Ornithologe()

    val franz = Jaeger()

    var markierungsziffer = 1
    for (vogel in voegel) {
        max.markiereVogel(vogel, markierungsziffer++)
        vogel.fliegen()
        if (vogel is Ente) {
            franz.jage(vogel)
        }

    }
}