package mvc_dialogfuerung.view

import mvc_dialogfuerung.model.Person

interface View {
    fun showInitialMenue()

    fun greetSelected(greetings: ArrayList<String>)
    fun amountOfPersonChanged(persons: ArrayList<Person>)

    fun addPersonMenueSelected()
    fun removePersonMenueSelected()
    fun showCommandNotFound(command:String)
}
