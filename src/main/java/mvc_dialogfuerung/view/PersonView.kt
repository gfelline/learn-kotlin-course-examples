package mvc_dialogfuerung.view

import mvc_dialogfuerung.model.Person
import mvc_dialogfuerung.controller.Controller
import mvc_dialogfuerung.controller.PersonController
import java.util.*

class PersonView : View {

    lateinit var controller: Controller

    fun setController(controller: PersonController) {
        this.controller = controller
    }

    override fun amountOfPersonChanged(persons: ArrayList<Person>) {
        println("""
                persons: $persons
                Commands:
                add
                remove
                greet
            """.trimIndent())
    }

    override fun showInitialMenue() {
        println("""
                Welcome to the best Greeter!
                Commands:
                add
                remove
                greet
            """.trimIndent())
        askForCommand()
    }

    override fun greetSelected(greetings: ArrayList<String>) {
        greetings.forEach { greeting ->
            println(greeting)
        }
    }

    override fun addPersonMenueSelected() {
        showFirstAndLastNameMenue()

    }

    override fun removePersonMenueSelected() {
        showFirstAndLastNameMenue()
    }

    fun showFirstAndLastNameMenue() {
        println("""
            Please write your firstname and lastname.
        """.trimIndent())
    }

    override fun showCommandNotFound(command:String) {
        println("Befehl $command nicht erkannt!")
    }

    private fun askForCommand() {
        askForCommand("")
    }

    private fun askForCommand(mode:String) {
        val scanner = Scanner(System.`in`)

        var command:String
        do {
            command = when {
                mode.isEmpty() -> scanner.nextLine()
                else -> scanner.nextLine() + mode
            }
        } while (controller.execute(command))
    }
}