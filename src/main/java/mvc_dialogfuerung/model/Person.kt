package mvc_dialogfuerung.model

data class Person(val firstName:String, val lastName:String) {

    fun greet() :String {
        return "Hello, $firstName $lastName"
    }
}