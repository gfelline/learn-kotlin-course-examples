package mvc_dialogfuerung.model.repository

import mvc_dialogfuerung.model.Person

class PersonInMemoryRepository : PersonRepository {

    val persons = arrayListOf<Person>()

    override fun createPerson(firstName: String, lastName: String): Person {
        val newPerson = Person(firstName, lastName)
        persons.add(newPerson)
        return newPerson
    }

    override fun removePerson(firstName: String, lastName: String) {
        val personToRemove = Person(firstName, lastName)
        persons.remove(personToRemove)
    }

    override fun getAllPersons(): ArrayList<Person> {
        return persons
    }

}