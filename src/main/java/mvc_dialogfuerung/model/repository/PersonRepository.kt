package mvc_dialogfuerung.model.repository

import mvc_dialogfuerung.model.Person

interface PersonRepository {

    fun createPerson(firstName: String, lastName: String): Person

    fun removePerson(firstName: String, lastName: String)

    fun getAllPersons(): ArrayList<Person>

    companion object {

        fun getRepo(): PersonRepository {
            return PersonInMemoryRepository()
        }

    }
}