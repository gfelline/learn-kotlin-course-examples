package mvc_dialogfuerung.model.repository

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import mvc_dialogfuerung.model.Person
import java.io.File

class PersonJsonRepository : PersonRepository {

    val gson = GsonBuilder().setPrettyPrinting().create()
    val listType = object : TypeToken<ArrayList<Person>>() {}.type
    val jsonFile = File("persons.json")

    override fun createPerson(firstName: String, lastName: String): Person {
        val newPerson = Person(firstName, lastName)
        val persons = getAllPersons()
        persons.add(newPerson)
        save(persons)
        return newPerson
    }

    override fun removePerson(firstName: String, lastName: String) {
        val persons = getAllPersons().filterNot { p ->
            p.firstName == firstName && p.lastName == lastName
        }

        save(persons)
    }

    fun save(persons: List<Person>) {
        if (!jsonFile.exists()) {
            jsonFile.createNewFile()
        }

        val personJsonText = gson.toJson(persons)
        jsonFile.writeText(personJsonText)
    }

    override fun getAllPersons(): ArrayList<Person> {
        if (!jsonFile.exists() || jsonFile.readLines().isEmpty()) {
            return arrayListOf()
        }

        val jsonText = readFile(jsonFile)

        return gson.fromJson(jsonText, listType)
    }

    private fun readFile(jsonFile: File): String {
        return jsonFile.bufferedReader().use { it.readText() }
    }

}