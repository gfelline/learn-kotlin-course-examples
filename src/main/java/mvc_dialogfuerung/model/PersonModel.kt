package mvc_dialogfuerung.model

import mvc_dialogfuerung.model.repository.PersonRepository
import mvc_dialogfuerung.view.View

class PersonModel(val view: View) : PersonFunctions {

    val personRepository = PersonRepository.getRepo()

    override fun greet() {
        val greetings = arrayListOf<String>()
        val persons = personRepository.getAllPersons()
        persons.forEach { person ->
            greetings.add(person.greet())
        }
        view.greetSelected(greetings)
    }

    override fun addPerson(firstName: String, lastName: String) {
        personRepository.createPerson(firstName, lastName)
        view.amountOfPersonChanged(personRepository.getAllPersons())
    }

    override fun removePerson(firstName: String, lastName: String) {
        personRepository.removePerson(firstName, lastName)
        view.amountOfPersonChanged(personRepository.getAllPersons())
    }


}