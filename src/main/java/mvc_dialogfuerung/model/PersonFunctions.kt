package mvc_dialogfuerung.model


interface PersonFunctions {

    fun addPerson(firstName: String, lastName:String)
    fun removePerson(firstName: String, lastName:String)
    fun greet()

}