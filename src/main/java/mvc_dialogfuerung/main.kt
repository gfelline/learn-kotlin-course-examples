import mvc_dialogfuerung.controller.PersonController
import mvc_dialogfuerung.model.PersonModel
import mvc_dialogfuerung.view.PersonView

fun main(args: Array<String>) {

    val personView = PersonView()
    val personModel = PersonModel(personView)
    val personController = PersonController(personModel, personView)
    personView.setController(personController)


    personView.showInitialMenue()
}