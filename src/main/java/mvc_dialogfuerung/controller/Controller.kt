package mvc_dialogfuerung.controller

interface Controller {
    fun execute(command:String) : Boolean
}