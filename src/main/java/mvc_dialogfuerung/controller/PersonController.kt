package mvc_dialogfuerung.controller

import mvc_dialogfuerung.controller.UIState.*
import mvc_dialogfuerung.model.PersonFunctions
import mvc_dialogfuerung.view.PersonView

enum class UIState {
    firstCommand, add, remove
}

class PersonController(val personModel: PersonFunctions, val personView: PersonView) : Controller {

    private var state = firstCommand

    override fun execute(command: String): Boolean {
        when {
            "greet" == command -> {
                personModel.greet()
                return true
            }
            "add" == command -> {
                personView.showFirstAndLastNameMenue()
                state = add
                return true
            }
            "remove" == command -> {
                personView.showFirstAndLastNameMenue()
                state = remove
                return true
            }
            command.matches(Regex("[A-Za-z ]+ [A-Za-z ]+")) -> {
                val name = parseFullName(command)
                when (state) {
                    add -> personModel.addPerson(name[0], name[1])
                    remove -> personModel.removePerson(name[0], name[1])
                }
                state = firstCommand
                return true
            }
            command.endsWith(":mode=removePerson") -> {
                val name = parseFullName(command.removeSuffix(":mode=addPerson"))
                personModel.removePerson(name[0], name[1])
                return true
            }
            else -> {
                personView.showCommandNotFound(command)
                return false
            }
        }
    }

    private fun parseFullName(command: String): List<String> {
        return command.split(" ")
    }

}