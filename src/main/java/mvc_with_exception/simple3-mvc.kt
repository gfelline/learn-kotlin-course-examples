package mvc_with_exception

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


interface View {
    fun showInitialMenue()

    fun showGreet(greetings: ArrayList<String>)
    fun amountOfPersonChanged(persons: ArrayList<Person>)
}

class PersonView() : View {
    override fun amountOfPersonChanged(persons: ArrayList<Person>) {
        println("""
                persons: $persons
                Commands:
                add [name]
                remove [name]
                greet
            """.trimIndent())
        askForCommand()
    }

    lateinit var controller: Controller

    fun setController(controller: PersonController) {
        this.controller = controller
    }


    override fun showInitialMenue() {
        println("""
                Welcome to the best Greeter!
                Commands:
                add [name]
                remove [name]
                greet
            """.trimIndent())
        askForCommand()
    }

    override fun showGreet(greetings: ArrayList<String>) {
        greetings.forEach { greeting ->
            println(greeting)
        }
        askForCommand()
    }

    fun showCommandNotFound(command:String) {
        println("Sorry, Befehl: $command nicht gefunden")
    }

    private fun askForCommand() {
        val scanner = Scanner(System.`in`)

        while (true) {
            val command = scanner.nextLine()
            try {
                controller.execute(command)
            } catch (e:CommandNotFoundException) {
                showCommandNotFound(command)
            }
        }
    }


}

interface Controller {
    fun execute(command:String)
}

class PersonController(val personModel: PersonFunctions) : Controller {
    override fun execute(command: String) {

        if ("greet" == command) {
            personModel.greet()
        } else if (command.startsWith("add ")) {
            personModel.addPerson(parseName(command))
        } else if (command.startsWith("remove ")) {
            personModel.removePerson(parseName(command))
        } else {
            throw CommandNotFoundException()
        }


    }

    private fun parseName(command: String) : String {
        return command.split(" ")[1]
    }

}


class CommandNotFoundException : Exception() {

}

interface PersonFunctions {

    fun addPerson(personName: String)
    fun removePerson(personName: String)
    fun greet()

}

/*
*
* Hier kommt euer AbstractModel -> Getränke AbstractModel, Karten AbstractModel, xxxx
*
* */
class PersonModel(val view: View) : PersonFunctions{

    val personRepository = PersonRepository.getRepo()

    override fun greet() {
        val greetings = arrayListOf<String>()
        val persons = personRepository.getAllPersons()
        persons.forEach { person ->
            greetings.add(person.greet())
        }
        view.showGreet(greetings)
    }

    override fun addPerson(personName: String) {
        personRepository.createPerson(personName)
        view.amountOfPersonChanged(personRepository.getAllPersons())
    }

    override fun removePerson(personName: String) {
        personRepository.removePerson(personName)
        view.amountOfPersonChanged(personRepository.getAllPersons())
    }


}

data class Person(val name:String) {

    fun greet() :String {
        return "Hello, $name"
    }
}



// Testen
fun main(args: Array<String>) {

    val personView = PersonView()
    val personModel = PersonModel(personView)
    val personController = PersonController(personModel)
    personView.setController(personController)


    personView.showInitialMenue()


}




interface PersonRepository {

    fun createPerson(personName: String) : Person

    fun removePerson(personName: String)

    fun getAllPersons() : ArrayList<Person>

    companion object {

        fun getRepo() : PersonRepository {
            return PersonJsonRepository()
        }

    }
}

class PersonJsonRepository : PersonRepository {

    val gson = GsonBuilder().setPrettyPrinting().create()
    val listType = object : TypeToken<ArrayList<Person>>() { }.type
    val jsonFile = File("persons.json")

    override fun createPerson(personName: String): Person {
        val newPerson = Person(personName)
        val persons = getAllPersons()
        persons.add(newPerson)
        save(persons)
        return newPerson
    }

    override fun removePerson(personName: String) {
        val persons= getAllPersons().filterNot { p -> p.name == personName }

        save(persons)
    }

    fun save (persons:List<Person>) {
        if (!jsonFile.exists()) {
            jsonFile.createNewFile()
        }

        val personJsonText = gson.toJson(persons)
        jsonFile.writeText(personJsonText)
    }

    override fun getAllPersons(): ArrayList<Person> {
        if (!jsonFile.exists() || jsonFile.readLines().isEmpty()) {
            return arrayListOf()
        }

        val jsonText = readFile(jsonFile)

        return gson.fromJson(jsonText, listType)
    }

    private fun readFile(jsonFile: File): String {
        return jsonFile.bufferedReader().use { it.readText() }
    }

}


class PersonInMemoryRepository : PersonRepository {

    val persons = arrayListOf<Person>()


    override fun createPerson(personName: String): Person {
        val newPerson = Person(personName)
        persons.add(newPerson)
        return newPerson
    }

    override fun removePerson(personName: String) {
        val personToRemove = Person(personName)
        persons.remove(personToRemove)
    }

    override fun getAllPersons(): ArrayList<Person> {
        return persons
    }

}

