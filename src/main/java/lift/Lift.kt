package lift

class Lift(val bezeichnung:String, val stockwerke:IntRange) {

    var currentStockwerk:Int = stockwerke.first

    fun fahreNach(stockwerk:Int) {

        when {
            stockwerk == currentStockwerk -> println("Dieser $bezeichnung ist bereits am Ziel")
            stockwerk in stockwerke -> {
                println("Dieser $bezeichnung fährt von $currentStockwerk nach $stockwerk")
                currentStockwerk = stockwerk
            }
            stockwerk !in stockwerke -> println("Dieser $bezeichnung ist bereits am Ziel")

        }

/*
        if (stockwerk != currentStockwerk) {
            if (stockwerk in stockwerke) {
                println("Dieser $bezeichnung fährt von $currentStockwerk nach $stockwerk")
                currentStockwerk = stockwerk
            } else {
                println("Dieser $bezeichnung kennt dieses Stockwerk nicht")
            }
        } else {
            println("Dieser $bezeichnung ist bereits am Ziel")
        }
*/


    }

}