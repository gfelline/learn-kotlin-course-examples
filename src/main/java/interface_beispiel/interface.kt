package interface_beispiel

class Schule {

    val schueler = arrayListOf<Schueler>()

    val mitarbeiter = Mitarbeiter()
}


interface Person {

    fun gehen()

    fun begruessen()

}


class Mitarbeiter : Person {
    override fun gehen() {
        println("gehe produktiv")
    }

    override fun begruessen() {
        println("Hoi!")
    }

}

class Schueler : Person {
    override fun gehen() {
        println("gehe lernend")
    }

    override fun begruessen() {
        print("Heiy ciao")
    }

}


fun main(args: Array<String>) {

    val personen = arrayListOf(Mitarbeiter(), Schueler())

    personen.forEach { person ->
        person.begruessen()
    }

}