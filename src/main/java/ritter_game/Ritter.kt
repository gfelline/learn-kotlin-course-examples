package ritter_game

class Ritter(val name:String, val augenfarbe:String) {

    init {
        println("Yuhuuu $name wurde gerade erstellt ;)!!!!!!")
    }


    var hp = 10

    fun atack(enemy:Ritter) {
        println("$name atackiert ${enemy.name}" )
        enemy.dropHP(5)
        println("${enemy.name} hat neu ${enemy.hp} health points!!!")
    }

    fun dropHP(amount:Int) {
        if (augenfarbe != "blau") {
            hp -= amount
            if (hp <= 0) {
                println("Game Over!!!!!!!")
            }
        }
    }

}