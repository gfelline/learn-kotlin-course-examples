package ritter_game

fun main(args: Array<String>) {


    val max = Ritter("max","blau")
    val hans = Ritter("hans","braun")
    val fritz = Ritter("fritz","grün")

    hans.atack(max)
    hans.atack(max)
    hans.atack(max)
    hans.atack(max)
    max.atack(hans)
    hans.atack(fritz)
    fritz.atack(hans)

}