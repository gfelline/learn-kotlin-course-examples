package mvc


import java.util.*


interface View {
    fun modelChanged()
}

class PersonView(val personModel: PersonModel, val controller: Controller) : View {

    init {
        personModel.registView(this)
    }

    override fun modelChanged() {
        show()
        askForCommand()
    }

    private fun askForCommand() {
        val scanner = Scanner(System.`in`)

        var command:String
        do {
            command = scanner.nextLine()
        } while (!controller.execute(command))

    }

    private fun show() {
        println("""
                Persons: ${personModel.persons}
                Commands:
                add [name]
                remove [name]
                greet
            """.trimIndent())
    }

}

abstract class Model {
    private val views = arrayListOf<View>()

    fun registView(view: View) {
        views.add(view)
    }

    fun changed() {

        views.forEach {view ->
            view.modelChanged()
        }
    }
}

interface Controller {
    fun execute(command:String) : Boolean
}

class PersonController(val personModel: PersonModel) : Controller {
    override fun execute(command: String): Boolean {

        if ("greet" == command) {
            personModel.greet()
            return true
        } else if (command.startsWith("add ")) {
            personModel.addPerson(parseName(command))
            return true
        } else if (command.startsWith("remove ")) {
            personModel.removePerson(parseName(command))
            return true
        }

        println("Befehl $command nicht erkannt!")

        return false
    }

    private fun parseName(command: String) : String {
        return command.split(" ")[1]
    }

}




/*
*
* Hier kommt euer Model -> Getränke Model, Karten Model, xxxx
*
* */
class PersonModel : Model() {

    val persons = arrayListOf<Person>()

    fun greet() {
        persons.forEach { person ->
            person.greet()
        }
        changed()
    }

    fun addPerson(personName: String) {
        persons.add(Person(personName))
        changed()
    }

    fun removePerson(personName: String) {
        persons.remove(Person(personName))
        changed()
    }


}

data class Person(val name:String) {

    fun greet() {
        println("Hello, $name")
    }
}



// Testen
fun main(args: Array<String>) {
    val personModel = PersonModel()

    val view = PersonView(personModel, PersonController(personModel))

    view.modelChanged()
}







