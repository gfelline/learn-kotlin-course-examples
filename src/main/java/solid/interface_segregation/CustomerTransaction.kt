package solid.interface_segregation

class CustomerTransaction : Reporting, Accounting {

    override fun getName() = println("Name ....")

    override fun getDate() = println("Date ....")

    override fun productBreakDown() = println("Product Breakdown ....")

    override fun prepareInvoice() = println("prepare invoice .....")

    override fun chargeCustomer() = println("charge cusotmer")

}


class AccountsReceivable(val transaction: Accounting) {
    fun use() {
        transaction.prepareInvoice()
        transaction.chargeCustomer()
    }
}

interface Accounting {
    fun prepareInvoice()
    fun chargeCustomer()
}

class ReportGenerator(val transaction: Reporting) {
    fun use() {
        transaction.getName()
        transaction.getDate()
        transaction.productBreakDown()
    }
}

interface Reporting {
    fun getName()


    fun getDate()
    fun productBreakDown()
}

fun main() {
    val reportGenerator = ReportGenerator(CustomerTransaction())
    reportGenerator.use()
}