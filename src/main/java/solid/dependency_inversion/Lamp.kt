package solid.dependency_inversion

import solid.dependency_inversion.DEVICE.*

class Button(val switchable: Switchable) {
    fun click() {
        when (switchable.isActive()){
            true -> switchable.deactivate()
            false -> switchable.activate()
        }
    }
}

class Button2() {

    val lamp = Lamp()

    fun click() {
        when (lamp.isActive()){
            true -> lamp.deactivate()
            false -> lamp.activate()
        }
    }
}

interface Switchable {
    fun activate()
    fun deactivate()
    fun isActive() : Boolean

    companion object instance {
        val switchable = Lamp()
    }
}

class Lamp : Switchable {
    private var on = false

    override fun activate() {
        turnOn()
    }

    override fun deactivate() {
        turnOff()
    }

    override fun isActive(): Boolean {
        return isOn()
    }

    private fun turnOn() {
        println("Turn on...")
        on = true
    }

    private fun turnOff() {
        println("Turn off...")
        on = false
    }

    private fun isOn() : Boolean {
        return on
    }
}

class Drill : Switchable {

    override fun activate() {
        println("drill... on")
    }

    override fun deactivate() {
        println("drill... off")
    }

    override fun isActive(): Boolean {
        return true
    }
}

fun main() {
    val button0 = Button(Drill())
    button0.click()
    button0.click()

    val button = Button(Switchable.switchable)
    button.click()
    button.click()

    val button2 = Button2()
    button2.click()
    button2.click()

    val enum = DRILL
    val button3 = ButtonFactory.create(enum)
    button3.click()
    button3.click()
}

object ButtonFactory {

    fun create(device: DEVICE) : Button {
        return when(device){
            LAMP -> Button(Lamp())
            DRILL -> Button(Drill())
        }
    }

}

enum class DEVICE {
    LAMP, DRILL
}