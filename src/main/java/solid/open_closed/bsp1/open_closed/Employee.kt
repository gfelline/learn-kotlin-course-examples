package solid.open_closed.bsp1.open_closed

open abstract class Employee(val id:Int,
               val name:String,
               val departement: String,
               val working: Boolean) {

    abstract fun performDuties()

}

class Doctor(id:Int,
             name:String,
             departement: String,
             working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a doctor!")
    }

    override fun performDuties() {
        prescribeMedicine()
        diagnosePatient()
    }

    private fun prescribeMedicine() {
        println("Prescribe Medicine")
    }

    private fun diagnosePatient() {
        println("Diagnosing Patient")
    }
}

class Nurse(id:Int,
             name:String,
             departement: String,
             working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a nurse!")
    }

    override fun performDuties() {
        checkVitalSigns()
        drawBlood()
        cleanPatientArea()
    }

    private fun checkVitalSigns() {
        println("checking vitals")
    }

    private fun drawBlood() {
        println("drawing blood")
    }

    private fun cleanPatientArea() {
        println("cleaning Patient Area")
    }
}

class Chef(id:Int,
            name:String,
            departement: String,
            working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a chef!")
    }

    override fun performDuties() {
        cook()
    }

    private fun cook() {
        println("cooking")
    }
}