package solid.open_closed.bsp1.open_closed

class HospitalManagemnt {

    fun callUpon(employee: Employee) {
        employee.performDuties()
    }
}
