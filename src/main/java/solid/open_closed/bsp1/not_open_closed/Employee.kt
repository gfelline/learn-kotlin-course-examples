package solid.open_closed.bsp1.not_open_closed

open class Employee(val id:Int,
               val name:String,
               val departement: String,
               val working: Boolean)

class Doctor(id:Int,
             name:String,
             departement: String,
             working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a doctor!")
    }
}

class Nurse(id:Int,
             name:String,
             departement: String,
             working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a nurse!")
    }
}

class Chef(id:Int,
            name:String,
            departement: String,
            working: Boolean) : Employee(id, name, departement, working) {
    init {
        println("I'm a chef!")
    }
}