package solid.open_closed.bsp1.not_open_closed

fun main() {

    val hospitalManagemnt = HospitalManagemnt()
    val susanne = Doctor(1,
            "Susanne",
            "Emergency",
            true)
    val max = Nurse(2,
            "Max",
            "Emergency",
            true)

    val paolo = Chef(3,
    "Paolo",
    "Kitchen",
    true)

    hospitalManagemnt.callUpon(max)
    hospitalManagemnt.callUpon(susanne)


}