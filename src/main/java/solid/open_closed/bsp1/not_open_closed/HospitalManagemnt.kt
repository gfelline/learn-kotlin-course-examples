package solid.open_closed.bsp1.not_open_closed

class HospitalManagemnt {

    fun callUpon(employee: Employee) {
        if (employee is Nurse) {
            checkVitalSigns()
            drawBlood()
            cleanPatientArea()
        } else if (employee is Doctor) {
            diagnosePatient()
            prescribeMedicine()
        } else if (employee is Chef) {
            cook()
        }
    }

    // Chef
    private fun cook() {
        println("cooking")
    }

    // Nurses
    private fun checkVitalSigns() {
        println("checking vitals")
    }

    private fun drawBlood() {
        println("drawing blood")
    }

    private fun cleanPatientArea() {
        println("cleaning Patient Area")
    }

    // Doctors
    private fun prescribeMedicine() {
        println("Prescribe Medicine")
    }

    private fun diagnosePatient() {
        println("Diagnosing Patient")
    }
}
