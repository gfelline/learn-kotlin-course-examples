package solid.open_closed.bsp2.not_open_closed

fun main(args: Array<String>) {

    val retangle = Retangle(12.0, 23.0)
    val bigRetangle = Retangle(200.0, 120.0)
    val circle = Circle(4.0)

    val areaCalculator = AreaCalculator()

    val calculateArea = areaCalculator.calculateArea(listOf(retangle, bigRetangle, circle))

    println("The calculated Area is: $calculateArea")

}