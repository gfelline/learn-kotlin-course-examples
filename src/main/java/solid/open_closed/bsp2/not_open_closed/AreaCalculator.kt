package solid.open_closed.bsp2.not_open_closed

class AreaCalculator {

    fun calculateArea(shapes: List<Shape>): Double {
        var area = 0.0
        for (shape in shapes) {
            area += shape.calculateArea()
        }
        return area
    }

}