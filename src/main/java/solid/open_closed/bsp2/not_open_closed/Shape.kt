package solid.open_closed.bsp2.not_open_closed

interface Shape {
    fun calculateArea() : Double
}

class Circle(val radius: Double) : Shape {
    override fun calculateArea() : Double {
        return radius * radius * Math.PI
    }
}

class Retangle(val width: Double, val height: Double) : Shape {
    override fun calculateArea() : Double {
        return width * height
    }
}

class Square(val side: Double) : Shape {
    override fun calculateArea(): Double {
        return side * side
    }
}