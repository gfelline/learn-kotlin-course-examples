package lampe

class Lampe(val bezeichnung:String) {
    var an = false

    fun schalteEin() {
        an = true
    }

    fun schalteAus() {
        an = false
    }

    fun istAn() {
        println("Lampe ist $an")
    }
}


