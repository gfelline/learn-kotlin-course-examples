package getter_setter

class Person(val name: String) {


    var istGutGelaunt = false

    var konto = 1000
        get() = field
        private set

    fun talk() {

    }

    fun zahleEin(betrag: Int) {
        if (betrag < 0)
            return

        konto += betrag
    }

}


fun main(args: Array<String>) {

    val max = Person("Max")
    val hans = Person("Hans")

    println("Die Laune von ${max.name} ist ${max.istGutGelaunt}")

    max.istGutGelaunt = true

    println("Die Laune von ${max.name} ist ${max.istGutGelaunt}")

    max.zahleEin(10)

    //max.konto = 19

    println("weil Konto = ${max.konto}")


}