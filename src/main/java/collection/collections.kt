package collection


fun main(args: Array<String>) {

    // List

    // leere Liste für zahlen

    val bereitsGefuelltMitZahlen = listOf(2,3,55,6,55)

    val nummern = mutableListOf<Int>()

    nummern.add(22)
    nummern.add(22)

    nummern.add(223)
    nummern.add(333)

    nummern.add(224)
    nummern.add(335)

    println(nummern)

    println(nummern[1])
    println(nummern.get(1))
    println(nummern[4])

    for (nummer in nummern) {
        println(nummer)
    }

    nummern.forEach {
        println(it)
    }

    nummern.filter {
        it < 100
    }.forEach {
        println("Coole nummer $it")
    }

    // Set

    val eineMenge = mutableSetOf(3,4,5,6,3,4,5)

    eineMenge.add(3)
    println(eineMenge)

    // map


    val eineMap = mutableMapOf(Pair(22,"Hallo"),Pair(2,"Bingo"))
    val eineMapBesser = mutableMapOf("22" to "Hallo",2 to "Bingo")

    val result = eineMapBesser["22"]

    println(result)


    println(eineMapBesser[22])

    eineMapBesser.forEach { key, value ->
        println("Key $key, Value $value")
    }

    for (entry in eineMapBesser.entries) {
        println("Key ${entry.key}, Value ${entry.value}")
    }






}