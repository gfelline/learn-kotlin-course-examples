package schule

fun main(args: Array<String>) {

    val harald = Dozent("Harald", 64)
    harald.setzeUnterrichtsfach("Deutsch")
    val fritz = Student("Fritz", 20)

    val teko = Schule("TEKO")
    teko.meldePersonAn(harald)
    teko.meldePersonAn(fritz)

    teko.gibNameUndAlterAllerPersonenAus()
    teko.gibNameUndAlterUndFachDozentenAus()


}