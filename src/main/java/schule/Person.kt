package schule

abstract class Person(val name:String, var alter:Int) {

    protected fun gibNameUndAlterAus() {
        println("$name ist $alter Jahre alt!")
    }

    abstract fun gibTaetigkeitAus()

    open protected fun gibEtwasSpeziellesAus() {

    }

    fun wasMachstDuHier() {
        gibNameUndAlterAus()
        gibEtwasSpeziellesAus()
        gibTaetigkeitAus()
    }

}