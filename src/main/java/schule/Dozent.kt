package schule

class Dozent(name:String, alter:Int) : Person(name,alter) {
    private var fach:String? = null

    fun setzeUnterrichtsfach(fach:String) {
        this.fach = fach
    }

    private fun gibFachAus()  {
        if (fach == null){
            println("$name unterrichtet zur Zeit kein Fach!")
        } else {
            println("$name unterrichtet $fach")
        }
    }

    override fun gibEtwasSpeziellesAus() {
        gibFachAus()
    }


    override fun gibTaetigkeitAus() {
        println("$name ist ein Dozent!")
    }


}