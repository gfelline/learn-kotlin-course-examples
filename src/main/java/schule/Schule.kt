package schule

class Schule(val name:String) {

    private val personen:MutableList<Person> = mutableListOf()

    fun meldePersonAn(person:Person) {
        personen.add(person)
        println("${person.name} wurde soeben an der $name Schule angemeldet!")
    }

    fun gibNameUndAlterAllerPersonenAus() {
        for (person in personen) {
            person.wasMachstDuHier()
        }
    }

    fun gibNameUndAlterUndFachDozentenAus() {
        /*
        personen.filter { person ->
            person is Dozent
        }.forEach {person ->
            person.wasMachstDuHier()
        }
        */
        for (person in personen) {
            if (person is Dozent) {
                person.wasMachstDuHier()
            }
        }
    }

}