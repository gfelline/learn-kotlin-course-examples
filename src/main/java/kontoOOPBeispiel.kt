fun main(args: Array<String>) {

    val kontoStefan = Konto("123", "Stefan", 100.0)
    kontoStefan.zahleEin(1000.0)

    val kontoRenato = Konto("124", "Renato", 100.0)

    val kontoThomas = Konto("125", "Thomas", 100.0)

    kontoStefan.zahleEin(-1000000.0)

}

class Konto(val ktoNummer: String, val ktoInhaber: String, startKapital: Double) {

    private var ktoStand:Double = 0.0

    init {
        println("Konto für $ktoInhaber mit einen Startkapital von $startKapital eröffnet")
        ktoStand += startKapital
    }

    fun zahleEin(betrag:Double) {
        if (betrag > 0) {
            ktoStand += betrag
            println("Einzahlung von $betrag für KontoInhaber $ktoInhaber")
            println("Neuer KtoStand beträgt $ktoStand")
        } else {
            println("Netter Versuch!!!!")
        }

    }

}


